import string


def generate_words_list(initial_text: str) -> list:
    """Generates the words_list if the default one is not used"""

    cleaned_text = (
        initial_text.replace("\n", " ")
        .lower()
        .translate(str.maketrans("", "", string.punctuation))  # strip punctuation
    )

    result = list(set(cleaned_text.split(" ")))
    result.sort()
    return result


def append_html_tag(string: str, tag: str, allowed_tags: list) -> str:
    if tag not in allowed_tags:
        raise ValueError(f"The tag {tag} is not allowed in this context.")

    return f"<{tag}>{string}</{tag}>"
