import random

from lorem.constants import WORDS_LIST
from lorem.utils import append_html_tag, generate_words_list


class LoremIpsum:
    def __init__(
        self, initial_text: str | None = None, seed: int | None = None
    ) -> None:
        if not initial_text:
            self.words_list = WORDS_LIST
        else:
            self.words_list = generate_words_list(initial_text)

        self.seed = seed
        self.title_min_length = 2
        self.title_max_length = 5

        self.sentence_min_length = 3
        self.sentence_max_length = 25

        self.paragraph_min_length = 1
        self.paragraph_max_length = 20

    def generate_string(self, min_length: int, max_length: int) -> str:
        """Returns a string based on the given parameters"""
        number_min, number_max = self.sanitize_variable_ranges(min_length, max_length)

        if self.seed:
            random.seed(self.seed)
        number = random.randint(number_min, number_max)

        if self.seed:
            random.seed(self.seed)
        return " ".join(random.choices(self.words_list, k=number)).capitalize()

    def sanitize_variable_ranges(self, number_min: int, number_max: int) -> tuple:
        """Check that the variable ranges are consistant"""
        if number_min < 1:
            raise ValueError("Minimum value should be a positive integer")

        if number_max < 1:
            raise ValueError("Maximum value should be a positive integer")

        if number_max > len(self.words_list):
            raise ValueError(f"Maximum allowed value: {self.words_list}")

        if number_min > number_max:
            raise ValueError("Minimum value cannot be superior to maximum value")

        return (number_min, number_max)

    def title(
        self,
        min_length: int | None = None,
        max_length: int | None = None,
        html_tag: str | None = None,
    ) -> str:
        """Returns a string meant to be used as a title"""
        if not min_length:
            min_length = self.title_min_length
        if not max_length:
            max_length = self.title_max_length

        result = self.generate_string(min_length, max_length)

        allowed_tags = ["p", "span", "h1", "h2", "h3", "h4", "h5", "h6"]
        if html_tag:
            result = append_html_tag(result, html_tag, allowed_tags)

        return result

    def sentence(
        self,
        min_length: int | None = None,
        max_length: int | None = None,
        html_tag: str | None = None,
    ) -> str:
        """Returns a string meant to be used as a full sentence"""
        if not min_length:
            min_length = self.sentence_min_length
        if not max_length:
            max_length = self.sentence_max_length

        result = self.generate_string(min_length, max_length) + "."

        allowed_tags = ["p", "span"]
        if html_tag:
            result = append_html_tag(result, html_tag, allowed_tags)

        return result

    def paragraph(
        self,
        min_sentence_length: int | None = None,
        max_sentence_length: int | None = None,
        min_sentences: int | None = None,
        max_sentences: int | None = None,
        html_tag: str | None = None,
    ) -> str:
        """Returns a series of sentences meant to be used as a full paragraph"""
        if not min_sentences:
            min_sentences = self.paragraph_min_length
        if not max_sentences:
            max_sentences = self.paragraph_max_length

        (
            sanitized_min_sentences,
            sanitized_max_sentences,
        ) = self.sanitize_variable_ranges(min_sentences, max_sentences)

        if self.seed:
            random.seed(self.seed)
        nb_sentences = random.randint(sanitized_min_sentences, sanitized_max_sentences)

        sentences = []
        for _ in range(nb_sentences):
            sentences.append(self.sentence(min_sentence_length, max_sentence_length))
        result = " ".join(sentences)

        allowed_tags = ["p", "span"]
        if html_tag:
            result = append_html_tag(result, html_tag, allowed_tags)

        return result
