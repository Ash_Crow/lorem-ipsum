import unittest

from lorem.lorem import LoremIpsum


class TestLoremIpsum(unittest.TestCase):
    def setUp(self):
        self.lorem = LoremIpsum()

    def test_generate_string_respects_limits(self):
        generated_string = self.lorem.generate_string(3, 5)

        assert isinstance(generated_string, str)

        assert 3 <= len(generated_string.split(" ")) <= 5

    def test_sanitize_variable_ranges_only_allows_positive_values(self):
        with self.assertRaises(ValueError):
            self.lorem.sanitize_variable_ranges(-10, 1)

        with self.assertRaises(ValueError):
            self.lorem.sanitize_variable_ranges(1, -5)

    def test_max_value_cannot_be_bigger_than_words_list(self):
        with self.assertRaises(ValueError):
            self.lorem.sanitize_variable_ranges(1, 300)

    def test_min_value_cannot_be_bigger_than_max_value(self):
        with self.assertRaises(ValueError):
            self.lorem.sanitize_variable_ranges(5, 3)

    def test_title(self):
        li = LoremIpsum(seed=42)
        self.assertEqual(li.title(html_tag="h3"), "<h3>Minim adipiscing</h3>")

    def test_sentence(self):
        li = LoremIpsum(seed=42)
        self.assertEqual(
            li.sentence(html_tag="span"),
            "<span>Minim adipiscing duis do occaecat nisi sunt anim ex adipiscing deserunt in adipiscing cupidatat minim irure deserunt laborum qui ad qui non enim.</span>",  # noqa
        )

    def test_paragraph(self):
        li = LoremIpsum(seed=42)
        self.assertEqual(
            li.paragraph(html_tag="p"),
            "<p>Minim adipiscing duis do occaecat nisi sunt anim ex adipiscing deserunt in adipiscing cupidatat minim irure deserunt laborum qui ad qui non enim. Minim adipiscing duis do occaecat nisi sunt anim ex adipiscing deserunt in adipiscing cupidatat minim irure deserunt laborum qui ad qui non enim. Minim adipiscing duis do occaecat nisi sunt anim ex adipiscing deserunt in adipiscing cupidatat minim irure deserunt laborum qui ad qui non enim. Minim adipiscing duis do occaecat nisi sunt anim ex adipiscing deserunt in adipiscing cupidatat minim irure deserunt laborum qui ad qui non enim.</p>",  # noqa
        )
