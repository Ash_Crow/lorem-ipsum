import unittest

from lorem.constants import RAW_SENTENCE, WORDS_LIST
from lorem.utils import append_html_tag, generate_words_list


class TestGenerateWordsList(unittest.TestCase):
    def test_generate_words_list_produces_expected_output(self):
        raw_sentence = "Dol'ore, exercitation!! Dolore, /laboris/."
        words_list = ["dolore", "exercitation", "laboris"]
        self.assertEqual(generate_words_list(raw_sentence), words_list)

        self.assertEqual(generate_words_list(RAW_SENTENCE), WORDS_LIST)


class TestAppendHtmlTag(unittest.TestCase):
    def test_append_html_tag_appends_allowed_tag(self):
        result = append_html_tag("excepteur incididunt", "p", ["p", "span"])
        self.assertEqual(result, "<p>excepteur incididunt</p>")

    def test_append_html_tag_refuses_non_allowed_tag(self):
        with self.assertRaises(ValueError):
            append_html_tag("excepteur incididunt", "script", ["p", "span"])


if __name__ == "__main__":
    unittest.main()
